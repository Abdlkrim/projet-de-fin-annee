from django.urls import path 
from . import views 
from django.contrib.auth import views as auth_view
urlpatterns = [
    path('test/', views.test , name='test' ) , 
    path('logout' , auth_view.LogoutView.as_view() , name='logout') , 
    path('test/add/<int:id>' , views.add , name='add') , 
    path('test/panier/' , views.panier , name='panier')
]
