from django import forms 
from .models import *
from django.contrib.auth.forms import UserCreationForm 
class UserForm(UserCreationForm) : 
    last_name = forms.CharField(max_length=16 , widget=forms.TextInput(attrs={'class': 'form-control' , 'placeholder' : 'Nom...'}))
    first_name = forms.CharField(max_length=16 , widget=forms.TextInput(attrs={'class': 'form-control' , 'placeholder' : 'Prenom...'}))
    username=forms.CharField(max_length=16 , widget=forms.TextInput(attrs={'class': 'form-control' , 'placeholder' : 'Username...'}))
    email = forms.CharField(max_length=50 , widget=forms.EmailInput(attrs={'class': 'form-control' , 'placeholder' : 'Email...'}))
    password1 = forms.CharField(max_length=16 , widget=forms.PasswordInput(attrs={'class': 'form-control' , 'placeholder' : 'Password...'}))
    password2 = forms.CharField(max_length=16 , widget=forms.PasswordInput(attrs={'class': 'form-control' , 'placeholder' : 'Confirm your Password...'}))
    adresse=forms.CharField(max_length=20 , widget=forms.TextInput(attrs={'class': 'form-control' , 'placeholder' : 'Adresse...'}))
    class Meta : 
        model = User 
        fields = ["username" , "last_name" , "first_name" , 'email' , 'password1' , "password2"]
class UserFormLogin(forms.Form) : 
    usernameL=forms.CharField(max_length=16 , widget=forms.TextInput(attrs={'class': 'form-control' , 'placeholder' : 'Username...'}))
    passwordL= forms.CharField(max_length=16 , widget=forms.PasswordInput(attrs={'class': 'form-control' , 'placeholder' : 'Password...'}))

