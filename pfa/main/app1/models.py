from django.db import models
from django.contrib.auth.models import User 
class AdresseUser(models.Model) : 
    adresse = models.CharField(max_length=55)
class Habite(models.Model) : 
    adresseH = models.ForeignKey(AdresseUser , related_name='adresseH' , on_delete=models.CASCADE )
    userH = models.ForeignKey(User , related_name='userH' , on_delete=models.CASCADE )
class Produit(models.Model):
    nameP = models.CharField(max_length=255)
    discriptionP =  models.CharField(max_length=255)
    prixP =  models.FloatField()
    image= models.CharField(max_length=50)
class Panier(models.Model):
    userC = models.ForeignKey(User , related_name='userC' , on_delete=models.CASCADE)
    produitC = models.ForeignKey(Produit , related_name='produitC' , on_delete=models.CASCADE )

